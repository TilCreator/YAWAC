#!/bin/ash
cp -R $( cd "$(dirname "$0")" ; pwd -P )/usr /
cp -R $( cd "$(dirname "$0")" ; pwd -P )/etc/init.d /etc/
chmod 755 /etc/init.d/yawac /usr/bin/yawac.sh

echo "Example config in ./etc/config/yawac (copy and edit in /etc/config/yawac)"
echo "/etc/init.d/yawac enable && /etc/init.d/yawac start" to start
