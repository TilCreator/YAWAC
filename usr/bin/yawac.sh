#!/bin/sh

# Yet Another Wifi Auto Connect (YAWAC)
# https://gitlab.com/TilCreator/YAWAC (https://github.com/mehdichaouch/YAWAC)

ConnCheckTimer=$(uci get yawac.@main[0].ConnCheckTimer)
NewConnCheckTimer=$(uci get yawac.@main[0].NewConnCheckTimer)
randMac=$(uci get yawac.@main[0].randMac)
ping_addr=$(uci get yawac.@main[0].ping_addr)
radio=$(uci get yawac.@main[0].radio)
interface=$(uci get yawac.@main[0].interface)
sta_ifname=$(uci get yawac.@main[0].sta_ifname)
sta_network=$(uci get yawac.@main[0].sta_network)

APP=YAWAC

net_status() {
    logger "$APP": Checking network...
    net=$(ping -I ${sta_ifname} -c5 ${ping_addr} | grep "time=")
    if [ "$net" ]; then
        logger "$APP": Network OK
        #got ping response!
        return
    else
        logger "$APP": Network failed. Starting network change...
        net_change
    fi
}

net_change() {
    logger "$APP": Performing network scan...
    scanres=

    while [ "$scanres" = "" ]; do
        #sometimes it shows nothing, so better to ensure we did a correct scan
        scanres=$(iwinfo $interface scan | grep SSID | cut -d'"' -f2)
    done

    killall -HUP hostapd
    logger "$APP": Searching available networks...

	uci set wireless.sta.disabled=1 &> /dev/null

    if [ "$1" ]; then
        ssid=$(uci get yawac.@wifi-iface[$n].ssid)
        eval ssid=\$$ssid
        echo Trying to connect to network "$1"":    $ssid"
        n=$(expr "$1" - "1")
    else
        n=-1
    fi

    while [ "1" ]; do
        n=$(expr "$n" + "1")

        if [ "$n" = "99" ]; then
            #too much counts. Crazy wireless count, breaking loop!
            break
        fi

        ssid=$(uci get yawac.@wifi-iface[$n].ssid)
        encryption=$(uci get yawac.@wifi-iface[$n].encryption)
        key=$(uci get yawac.@wifi-iface[$n].key)
        macaddr=$(uci get yawac.@wifi-iface[$n].macaddr)

        if [ "$ssid" = "" ]; then
            #ssid not existing or empty. Assume it's the end of the wlist file
            break
        fi

        active=$(echo $scanres | grep " $ssid ">&1 )
        if [ "$active" ]; then
            if [ "$1" ]; then
                echo Network found. Connecting...
            fi
            logger "$APP": "$ssid" network found. Applying settings..

			uci get wireless.@wifi-iface[$i] &> /dev/null
		    if [[ $? == 1 ]]; then
				uci add wireless wifi-iface
			fi

			uci set wireless.sta.disabled=0

			if [ "$(uci set wireless.sta.ssid)" != "$ssid" ]; then
	            uci set wireless.sta="wifi-iface"
	            uci set wireless.sta.ssid="$ssid"
	            uci set wireless.sta.encryption="$encryption"
	            uci set wireless.sta.device="$radio"
	            uci set wireless.sta.mode="sta"
	            uci set wireless.sta.ifname="$sta_ifname"
	            uci set wireless.sta.network="$sta_network"
	            uci set wireless.sta.key="$key"
	            uci set wireless.sta.macaddr="$macaddr"

	            uci commit wireless
	            #/etc/init.d/network restart
				wifi reload
			fi

            #wait some seconds for everything to connect and configure
            sleep $NewConnCheckTimer
            logger "$APP": Checking connectivity...

            #check for internet connection, 5 ping sends
            net=$(ping -I ${sta_ifname} -c5 ${ping_addr} | grep "time=")
            if [ "$net" ]; then
                #got ping response!
                logger "$APP": Internet working! Searching ended
                if [ "$1" ]; then
                    echo Success!
                fi
                    break
            fi
            #connect to freewifi
            if [ "$1" ]; then
                echo Connection failed!
                break
            fi
            logger "$APP": Failed! Searching next available network...
        fi
    done
}

#get_wifi_iface_n() {
#	for i in $(seq 0 99); do
#		uci get wireless.@wifi-iface[$i] &> /dev/null
#	    if [[ $? == 1 ]] || [[ "$(uci get wireless.@wifi-iface[$i].mode)" == "sta" ]]; then
#			wifi_iface_n=$i
#			break
#	    fi
#	done
#}

rand_mac_addr() {
    macaddr=$(dd if=/dev/urandom bs=1024 count=1 2>/dev/null|md5sum|sed 's/^\(..\)\(..\)\(..\)\(..\)\(..\)\(..\).*$/00:\2:\3:\4:\5:01/')
    # uci set wireless.$radio.macaddr="$macaddr"
    uci set wireless.sta.macaddr="$macaddr"
    uci commit wireless
    wifi reload
}

if [ "$1" = "" ]; then
    echo "No arguments supplied"
elif [ "$1" = "--force" ]; then
	#get_wifi_iface_n

    net_change $2
elif [ "$1" = "--daemon" ]; then
	#get_wifi_iface_n

    if [ "$randMac" = "1" ]; then
        rand_mac_addr
    fi

    net_change

    while [ "1" ]; do
        sleep $ConnCheckTimer
        #get_wifi_iface_n
        net_status
    done
else
    echo "Wrong arguments"
fi
